/* 
 * File:   usart.h
 * Author: labemb
 *
 * Created on 21 listopada 2015, 16:50
 */

#ifndef USART_H
#define	USART_H
#include <xc.h>           // lub htc.h f
#ifdef	__cplusplus
extern "C" {
#endif
    void configure_usart();
    void send_data_usart();



#ifdef	__cplusplus
}
#endif

#endif	/* USART_H */

