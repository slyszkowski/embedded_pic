
// Alicja Błachowicz
// Sławomir Łyszkowski

#pragma config OSC=HS, LVP=OFF, WDT=OFF, MCLRE=ON 

#include <xc.h>           // lub htc.h f

#include <pic18f8722.h>
#include "usart.h"

#define _XTAL_FREQ  10000000 

bit led_state = 0;
volatile unsigned long miliseconds = 0;
//deklaracje funkcji
void configure_led();
void switch_led();
void configure_timer();
void enable_interrupts();
unsigned long get_miliseconds();

//funkcja main

/*
void main_1(void) {
    configure_led();
    PORTDbits.RD0 = 1; // zapalenie LED 
    __delay_500ms();
    PORTDbits.RD0 = 0; // wygaszenie LED 
    while (1);
}*/

/*
void main2(void) {
    configure_led();
    while (1) {
        switch_led();
        __delay_500ms();
    }
} */
/*void main3(void){
    unsigned long curr_miliseconds = 0;
    unsigned long prev_miliseconds = 0;
    configure_led();
    configure_timer();
    enable_interrupts();
    while (1) {
        curr_miliseconds = get_miliseconds();
        if (curr_miliseconds - prev_miliseconds == 500) {
            switch_led();
            prev_miliseconds = curr_miliseconds;
        }
    }
}*/

//void main4(void)
void main(void)
{
    configure_usart();
    send_data_usart();
    while(1)
    {
    }
}

//definicje funkcji

void configure_led() {
    MEMCONbits.EBDIS = 1;
    TRISDbits.TRISD0 = 0; // linia 0 b 
}

void switch_led() {
    led_state = ~led_state;
    PORTDbits.RD0 = led_state;
}

void configure_timer() {
    T2CONbits.TOUTPS = 0b0101;
    T2CONbits.T2CKPS = 0b01;
    PR2 = 125;
    TMR2IF = 0;
    TMR2IE = 1;
    TMR2ON = 1;
}

void enable_interrupts() {
    GIE = 1;
    PEIE = 1;
}

unsigned long get_miliseconds() {
    unsigned long resu = 0;
    GIE = 0;
    resu = miliseconds;
    GIE = 1;
    return resu;
}

void interrupt isr(void) {
    if (TMR2IE && TMR2IF) {
        TMR2IF = 0;
        miliseconds++;

        return;
    }
}
