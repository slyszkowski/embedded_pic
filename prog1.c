// Alicja Błachowicz
// Sławomir Łyszkowski

#pragma config OSC=HS, LVP=OFF, WDT=OFF, MCLRE=ON 

#include <xc.h>           // lub htc.h f

//#include <pic18f8722.h>

#define _XTAL_FREQ  10000000 

//deklaracje funkcji
void __delay_500ms(void);
void configure_led();
bit led_state = 0;
void switch_led();

//funkcja main
void main_1(void) {
    configure_led();
    PORTDbits.RD0 = 1; // zapalenie LED 
    __delay_500ms();
    PORTDbits.RD0 = 0; // wygaszenie LED 
    while (1);
}

void main(void) {
	configure_led();
    while (1) {
        switch_led();
        __delay_500ms();
    }
}

//definicje funkcji
void configure_led() {
    MEMCONbits.EBDIS = 1;
    TRISDbits.TRISD0 = 0; // linia 0 b 
}

void switch_led() {
    led_state = ~led_state;
    PORTDbits.RD0 = led_state;
}

void __delay_500ms(void) { // funkcja opóźniająca o 500ms (10x50ms)
    for (unsigned char i = 0; i < 10; i++) {
        __delay_ms(50); // opó
    }
}